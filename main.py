# TEST FILE
from constants import DEFAULT_X
def double(x: int) -> int:
    return x * 2

def main(x):
    y = double(x)
    print(y)

main(DEFAULT_X)